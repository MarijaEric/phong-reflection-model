# Phong reflection model

Implementacija osnovnih elemenara računarske grafike, sa akcentom na osvetljenju. 

Ideja projekta je da se koristi za bolje razumevanje efekata koji se dobijaju kombinovanjem različitih vrsta osvetljenja.

Od naprednih tehnika su implementirani Blinn-Phong model i blending.

# Način korišćenja


1. Ukljucivanje/Iskljucivanje direkcionog svetla: D

2. Ukljucivanje/Iskljucivanje tačkastog svetla: S

3. Ukljucivanje/Iskljucivanje lampe : P

4. Ukljucivanje/Iskljucivanje Blinn-Phongovog modela : B

5. Pomeranje kamere : UP, DOWN, LEFT, RIGHT

6. Interaktivno nameštanje parametara specifičnog svetla.
