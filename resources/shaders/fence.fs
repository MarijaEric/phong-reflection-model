#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D rainTex;

void main()
{
    vec4 texColor = texture(rainTex, TexCoords);
    if(texColor.a < 0.5)
        discard;
    FragColor = texColor;
}