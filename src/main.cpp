#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <learnopengl/filesystem.h>
#include <learnopengl/shader_m.h>
#include <learnopengl/camera.h>
#include <learnopengl/model.h>
#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
unsigned int loadTexture(const char *path, bool gammaCorrection);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 10.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
double deltaTime = 0.0f;
double lastFrame = 0.0f;

// lighting
struct SpotLight{

    glm::vec3 position;
    glm::vec3 direction;
    float cutOff;
    float outerCutOff;

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;

    float constant;
    float linear;
    float quadratic;
};
struct PointLight {
    glm::vec3 position;

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
};


bool blinn = false;
bool blinnKeyPressed = false;

int spot=0;
bool spotKeyPressed = false;

int point=1;
bool pointKeyPressed = false;

int directional = 1;
bool directionalKeyPressed = false;

struct LightState {
    DirectionalLight dirLight;
    PointLight pointLight;
    SpotLight spotLight;

    void SaveToFile(std::string filename);
    void LoadFromFile(std::string filename);
};

void LightState::SaveToFile(std::string filename) {
    std::ofstream out(filename);
    out << dirLight.direction.x << '\n'
        << dirLight.direction.y << '\n'
        << dirLight.direction.z << '\n'
        << dirLight.ambient.x << '\n'
        << dirLight.ambient.y << '\n'
        << dirLight.ambient.z << '\n'
        << dirLight.diffuse.x << '\n'
        << dirLight.diffuse.y << '\n'
        << dirLight.diffuse.z << '\n'
        << dirLight.specular.x << '\n'
        << dirLight.specular.y << '\n'
        << dirLight.specular.z << '\n'

        << pointLight.position.x << '\n'
        << pointLight.position.y << '\n'
        << pointLight.position.z << '\n'
        << pointLight.ambient.x << '\n'
        << pointLight.ambient.y << '\n'
        << pointLight.ambient.z << '\n'
        << pointLight.diffuse.x << '\n'
        << pointLight.diffuse.y << '\n'
        << pointLight.diffuse.z << '\n'
        << pointLight.specular.x << '\n'
        << pointLight.specular.y << '\n'
        << pointLight.specular.z << '\n'
        << pointLight.constant << '\n'
        << pointLight.linear << '\n'
        << pointLight.quadratic << '\n'

        << spotLight.position.x << '\n'
        << spotLight.position.y << '\n'
        << spotLight.position.z << '\n'
        << spotLight.direction.x << '\n'
        << spotLight.direction.y << '\n'
        << spotLight.direction.z << '\n'
        << spotLight.cutOff << '\n'
        << spotLight.outerCutOff << '\n'
        << spotLight.ambient.x << '\n'
        << spotLight.ambient.y << '\n'
        << spotLight.ambient.z << '\n'
        << spotLight.diffuse.x << '\n'
        << spotLight.diffuse.y << '\n'
        << spotLight.diffuse.z << '\n'
        << spotLight.specular.x << '\n'
        << spotLight.specular.y << '\n'
        << spotLight.specular.z << '\n'
        << spotLight.constant << '\n'
        << spotLight.linear << '\n'
        << spotLight.quadratic << '\n'

            ;
}

void LightState::LoadFromFile(std::string filename) {
    std::ifstream in(filename);
    if (in) {
        in
                >> dirLight.direction.x
                >> dirLight.direction.y
                >> dirLight.direction.z
                >> dirLight.ambient.x
                >> dirLight.ambient.y
                >> dirLight.ambient.z

                >> dirLight.diffuse.x
                >> dirLight.diffuse.y
                >> dirLight.diffuse.z
                >> dirLight.specular.x
                >> dirLight.specular.y
                >> dirLight.specular.z

                >> pointLight.position.x
                >> pointLight.position.y
                >> pointLight.position.z
                >> pointLight.ambient.x
                >> pointLight.ambient.y
                >> pointLight.ambient.z
                >> pointLight.diffuse.x
                >> pointLight.diffuse.y
                >> pointLight.diffuse.z
                >> pointLight.specular.x
                >> pointLight.specular.y
                >> pointLight.specular.z
                >> pointLight.constant
                >> pointLight.linear
                >> pointLight.quadratic

                >> spotLight.position.x
                >> spotLight.position.y
                >> spotLight.position.z
                >> spotLight.direction.x
                >> spotLight.direction.y
                >> spotLight.direction.z
                >> spotLight.cutOff
                >> spotLight.outerCutOff
                >> spotLight.ambient.x
                >> spotLight.ambient.y
                >> spotLight.ambient.z
                >> spotLight.diffuse.x
                >> spotLight.diffuse.y
                >> spotLight.diffuse.z
                >> spotLight.specular.x
                >> spotLight.specular.y
                >> spotLight.specular.z
                >> spotLight.constant
                >> spotLight.linear
                >> spotLight.quadratic

                ;
    }
}
LightState *lightState;

void DrawImGui(LightState *lightState);
int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "PhongModel", nullptr, nullptr);
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    lightState = new LightState;
    lightState->LoadFromFile("resources/light_state.txt");

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    // Init Imgui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void) io;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);

    // build and compile our shader program
    // ------------------------------------
    Shader lightingShader("resources/shaders/box.vs", "resources/shaders/box.fs");
    Shader helicopterShader("resources/shaders/helicopter.vs", "resources/shaders/helicopter.fs");
    Shader genericShader("resources/shaders/generic.vs", "resources/shaders/generic.fs");
    Shader fenceShader("resources/shaders/fence.vs", "resources/shaders/fence.fs");
    Shader lampShader("resources/shaders/light.vs", "resources/shaders/light.fs");

    //--------------------------------------
    float boxVertices[] = {
            -0.5f, -0.5f, 0.5f, 0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  1.0f,
            -0.5f,  0.5f, 0.5f, 0.0f,  1.0f,

            -0.5f, -0.5f, -0.5f, 0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  1.0f,
            -0.5f,  0.5f, -0.5f, 0.0f,  1.0f
            };
    unsigned int boxIndices[] = {
            0, 2, 3,
            0, 1, 2,
            2, 6, 7,
            2, 3, 7,
            6, 4, 5,
            6, 4, 7,
            4, 0, 1,
            4, 1, 5,
            0, 4, 7,
            0, 7, 3,
            1, 5, 6,
            1, 6, 2,
    };

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(boxVertices), boxVertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(boxIndices), boxIndices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // load textures (we now use a utility function to keep the code more organized)
    // -----------------------------------------------------------------------------
    unsigned int diffuseMap = loadTexture(FileSystem::getPath("resources/textures/rocks.jpg").c_str(), false);
    unsigned int specularMap = loadTexture(FileSystem::getPath("resources/textures/rocks_spec.jpg").c_str(), false);
    unsigned int rainTexture = loadTexture(FileSystem::getPath("resources/textures/fence.png").c_str(), false);


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float vertices[] = {
            // positions          // normals           // texture coords
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
    };

    // first, configure the cube's VAO (and VBO)
    unsigned int VBO1, cubeVAO;
    glGenVertexArrays(1, &cubeVAO);
    glGenBuffers(1, &VBO1);

    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(cubeVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);


    unsigned int VAO_L;
    glGenVertexArrays(1, &VAO_L);
    glBindVertexArray(VAO_L);

    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);


    float fenceVertices[] = {
            -0.5f, -0.5f, 0.5f, 0.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  1.0f,
            -0.5f,  0.5f, 0.5f, 0.0f,  1.0f,

            -0.5f, -0.5f, 0.5f, 0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  1.0f
    };

    unsigned int VBO_R, VAO_R;
    glGenVertexArrays(1, &VAO_R);
    glGenBuffers(1, &VBO_R);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_R);
    glBufferData(GL_ARRAY_BUFFER, sizeof(fenceVertices), fenceVertices, GL_STATIC_DRAW);

    glBindVertexArray(VAO_R);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);


    // load model
    Model ourModel(FileSystem::getPath("resources/objects/helecopter/chopper.obj"));
    ourModel.SetShaderTextureNamePrefix("material.");


    // shader configuration
    // --------------------
    lightingShader.use();
    lightingShader.setInt("material.diffuse", 0);
    lightingShader.setInt("material.specular", 1);

    genericShader.use();
    genericShader.setInt("texture1", 0);

    fenceShader.use();
    fenceShader.setInt("rainTex", 0);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        double currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // be sure to activate shader when setting uniforms/drawing objects
        lightingShader.use();
        //lightingShader.setVec3("light.direction", glm::vec3(-0.2f, -1.0f, -0.3f));

        //helicopterShader.use();
        glm::mat4 m = glm::mat4(1.0f);
        m = glm::translate(m, glm::vec3(3*sin(float(glfwGetTime())), 3.0f + 0.01*glfwGetTime(), 3*cos(float(glfwGetTime()))));
        m = glm::rotate(m, (float)glfwGetTime(), glm::vec3(0.0f, 1.0f, 0.0f));

        lightingShader.setVec3("pointLight.position", lightState->pointLight.position);
        lightingShader.setVec3("pointLight.ambient", lightState->pointLight.ambient);
        lightingShader.setVec3("pointLight.diffuse", lightState->pointLight.diffuse);
        lightingShader.setVec3("pointLight.specular", lightState->pointLight.specular);
        lightingShader.setFloat("pointLight.constant", lightState->pointLight.constant);
        lightingShader.setFloat("pointLight.linear", lightState->pointLight.linear);
        lightingShader.setFloat("pointLight.quadratic", lightState->pointLight.quadratic);

        lightingShader.setVec3("dirLight.direction", lightState->dirLight.direction);
        lightingShader.setVec3("dirLight.ambient", lightState->dirLight.ambient);
        lightingShader.setVec3("dirLight.diffuse", lightState->dirLight.diffuse);
        lightingShader.setVec3("dirLight.specular", lightState->dirLight.specular);

        lightingShader.setVec3("spotLight.position", camera.Position);
        lightingShader.setVec3("spotLight.direction", camera.Front);
        lightingShader.setFloat("spotLight.cutOff", glm::cos(glm::radians(lightState->spotLight.cutOff)));
        lightingShader.setFloat("spotLight.outerCutOff", glm::cos(glm::radians(lightState->spotLight.outerCutOff)));

        lightingShader.setVec3("spotLight.ambient", lightState->spotLight.ambient);
        lightingShader.setVec3("spotLight.diffuse", lightState->spotLight.diffuse);
        lightingShader.setVec3("spotLight.specular", lightState->spotLight.specular);

        lightingShader.setFloat("spotLight.constant", lightState->spotLight.constant);
        lightingShader.setFloat("spotLight.linear", lightState->spotLight.linear);
        lightingShader.setFloat("spotLight.quadratic", lightState->spotLight.quadratic);



        lightingShader.setInt("directional", directional);
        lightingShader.setInt("spot", spot);
        lightingShader.setInt("point", point);

        // material properties
        lightingShader.setFloat("material.shininess", 64.0f);
        lightingShader.setBool("blinn", blinn);


        // view/projection transformations
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera.GetViewMatrix();
        lightingShader.setMat4("projection", projection);
        lightingShader.setMat4("view", view);

        // world transformation

        glm::mat4 model = glm::mat4(1.0f);
        lightingShader.setMat4("model", model);


        // bind diffuse map
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseMap);
        // bind specular map
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, specularMap);

        // render the cube
        glBindVertexArray(cubeVAO);
        for (int i= -1; i<2; i++) {
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(1.0 * i, 0.0f, 0.0f));
            //model = glm::rotate(model, 20.0f*i, glm::vec3(1.0, 0.3, 0.5));
            lightingShader.setMat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
        for (int i= -1; i<1; i++) {
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(1.0 * i + 0.4f, 1.0f, 0.0f));
            //model = glm::rotate(model, 20.0f*i, glm::vec3(1.0, 0.3, 0.5));
            lightingShader.setMat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
        genericShader.use();
        genericShader.setMat4("view", view);
        genericShader.setMat4("projection", projection);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseMap);
        glBindVertexArray(VAO);

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 2.0f, 0.0f));
        genericShader.setMat4("model", model);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);



        fenceShader.use();
        fenceShader.setMat4("view", view);
        fenceShader.setMat4("projection", projection);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, rainTexture);
        glBindVertexArray(VAO_R);

        for(int i = -6; i< 8; i++) {
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(1.0f*i, 0.0f, 0.0f));
            fenceShader.setMat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }
        helicopterShader.use();
        helicopterShader.setVec3("pointLight.position", lightState->pointLight.position);
        helicopterShader.setVec3("pointLight.ambient", lightState->pointLight.ambient);
        helicopterShader.setVec3("pointLight.diffuse", lightState->pointLight.diffuse);
        helicopterShader.setVec3("pointLight.specular", lightState->pointLight.specular);
        helicopterShader.setFloat("pointLight.constant", lightState->pointLight.constant);
        helicopterShader.setFloat("pointLight.linear", lightState->pointLight.linear);
        helicopterShader.setFloat("pointLight.quadratic", lightState->pointLight.quadratic);
        helicopterShader.setVec3("viewPosition", camera.Position);
        helicopterShader.setFloat("material.shininess", 32.0f);

        helicopterShader.setVec3("dirLight.direction", lightState->dirLight.direction);
        helicopterShader.setVec3("dirLight.ambient", lightState->dirLight.ambient);
        helicopterShader.setVec3("dirLight.diffuse", lightState->dirLight.diffuse);
        helicopterShader.setVec3("dirLight.specular", lightState->dirLight.specular);


        helicopterShader.setInt("direction", directional);

        helicopterShader.setInt("point", point);

        // view/projection transformations
        projection = glm::perspective(glm::radians(camera.Zoom),
                                      (float) SCR_WIDTH / (float) SCR_HEIGHT, 0.1f, 100.0f);

        view = camera.GetViewMatrix();

        helicopterShader.setMat4("projection", projection);
        helicopterShader.setMat4("view", view);

        // render the loaded model

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(3*sin(float(glfwGetTime())), 3.0f + 0.0001*glfwGetTime(), 3*cos(float(glfwGetTime()))));
        model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(0.0f, -1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5));

        helicopterShader.setMat4("model", model);

        ourModel.Draw(helicopterShader);


        lampShader.use();
        lampShader.setMat4("view", view);
        lampShader.setMat4("projection", projection);

        glBindVertexArray(VAO_L);
        model = glm::mat4(1.0f);
        model = glm::translate(model, lightState->pointLight.position);
        model = glm::scale(model, glm::vec3(0.3f));
        lampShader.setMat4("model", model);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        DrawImGui(lightState);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }


    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    lightState->SaveToFile("resources/light_state.txt");
    delete lightState;
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);

    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS  && !spotKeyPressed){
        spot = abs(1-spot);
        spotKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE)
    {
        spotKeyPressed = false;
    }

    if(glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS  && !pointKeyPressed){
        point = abs(1-point);
        pointKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE)
    {
        pointKeyPressed = false;
    }

    if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS  && !directionalKeyPressed){
        directional = abs(1-directional);
        directionalKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_RELEASE)
    {
        directionalKeyPressed = false;
    }

    if(glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS  && !blinnKeyPressed){
        blinn = !blinn;
        blinnKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE)
    {
        blinnKeyPressed = false;
    }

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    /*
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
    */
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    //camera.ProcessMouseScroll(yoffset);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture(char const * path, bool gammaCorrection)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
        {
            format = GL_RED;
        }
        else if (nrComponents == 3)
        {
            format = GL_RGB;
        }else {
            format = GL_RGBA;
            std::cout << "rgba" << std::endl;

        }
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

void DrawImGui(LightState *lightState) {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    {
        static float f = 0.0f;
        ImGui::Begin("Directional Light");
        bool on = directional==1;
        ImGui::Checkbox("Direction light on", &on);
        ImGui::DragFloat3("Direction vector", (float*)&lightState->dirLight.direction);
        ImGui::DragFloat3("Ambient", (float*)&lightState->dirLight.ambient);
        ImGui::DragFloat3("Diffuse", (float*)&lightState->dirLight.diffuse);
        ImGui::DragFloat3("Specular", (float*)&lightState->dirLight.specular);


        ImGui::End();
    }

    {
        ImGui::Begin("Point light");
        bool on = point==1;
        ImGui::Checkbox("Point light on", &on);
        ImGui::DragFloat3("Position", (float*)&lightState->pointLight.position);
        ImGui::DragFloat3("Ambient", (float*)&lightState->pointLight.ambient);
        ImGui::DragFloat3("Diffuse", (float*)&lightState->pointLight.diffuse);
        ImGui::DragFloat3("Specular", (float*)&lightState->pointLight.specular);
        ImGui::DragFloat("Constant", (float*)&lightState->pointLight.constant);
        ImGui::DragFloat("Linear", (float*)&lightState->pointLight.linear);
        ImGui::DragFloat("Quadratic", (float*)&lightState->pointLight.quadratic);

        ImGui::End();
    }
    {
        {
            ImGui::Begin("Spot light");
            bool on = spot==1;
            ImGui::Checkbox("Spot light on", &on);
            ImGui::DragFloat3("Position", (float*)&lightState->spotLight.position);
            ImGui::DragFloat3("Direction", (float*)&lightState->spotLight.position);
            ImGui::SliderFloat("cutOff", (float*)&lightState->spotLight.cutOff, 10.0f, 40.0f);
            ImGui::SliderFloat("outerCutOff", (float*)&lightState->spotLight.outerCutOff,
                               (*(float*)&lightState->spotLight.cutOff + 1.0f), 40.0f);

            ImGui::DragFloat3("Ambient", (float*)&lightState->spotLight.ambient);
            ImGui::DragFloat3("Diffuse", (float*)&lightState->spotLight.diffuse);
            ImGui::DragFloat3("Specular", (float*)&lightState->spotLight.specular);

            ImGui::DragFloat("Constant", (float*)&lightState->spotLight.constant);
            ImGui::DragFloat("Linear", (float*)&lightState->spotLight.linear);
            ImGui::DragFloat("Quadratic", (float*)&lightState->spotLight.quadratic);

            ImGui::End();
        }
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
